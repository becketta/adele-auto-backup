#!/bin/bash -l

#----------------------------------------------------------------------
# Make sure the Media drive is mounted
#----------------------------------------------------------------------
media_drive="$(diskutil list | grep Media | awk '{print $NF}' | sed 's/s[0-9]//')"
diskutil mountDisk $media_drive

# path to the Media drive
MEDIA="/Volumes/Media"


#----------------------------------------------------------------------
# Initialize new backup log file entry
#----------------------------------------------------------------------
{
    echo "----------------------------------------------------------------------"
    echo "DATE: $(date)"
    echo
    echo "Starting backup of Media Drive..."
} >> $MEDIA/.backuplog


#----------------------------------------------------------------------#
# Sync the Media Drive with the server                                 #
#----------------------------------------------------------------------#
# 
#----------------------------------------------------------------------
#--- Declare variables ---
#
# ignore Mac droppings and the backup log files
EXCLUDES="--exclude=auto-backup \
    --exclude=.DS_Store \
    --exclude=.Trash \
    --exclude=Cache \
    --exclude=Caches \
    --exclude=.backuplog \
    --exclude=.Trashes \
    --exclude=.fseventsd \
    --exclude=.TemporaryItems \
    --exclude=.Spotlight-V100 \
    --exclude=.DocumentRevisions-V100"

#----------------------------------------------------------------------
#--- Sync the Archive folder ---
#
# Archive directory
ARC_DIRS="$MEDIA/Archive/"
#
# Options to pass to rsync for archive folders
ARC_OPTS="-avAH --specials --remove-source-files"
#
# Backup directory for archive folders
ARC_BACKUPDIR="hanxiaox@hpcc.msu.edu:/mnt/research/icer-media-archive"
#
# Path to the Server's Restore directory
SERVER_RESTORE_DIR="hanxiaox@hpcc.msu.edu:/mnt/research/icer-media-archive/Restore"
#
# Path to the User's Restore directory
USER_RESTORE_DIR="$MEDIA/Restored_Files"

# Backup the Archive folders and record progress in the log file
{
    echo "Starting backup of Archive folders..."
    # Build and run command
    rsync $ARC_OPTS $EXCLUDES $ARC_DIRS $ARC_BACKUPDIR
    exit_status=$?
    if [ $exit_status -ne 0 ]; then
	echo "*** ERROR ***: An rsync error occured while backing up Archive folders."
	echo "               rsync Exit Value: $exit_status"
    fi
    echo "Finished backup of Archive folders."
} >> $MEDIA/.backuplog 2>&1

# Restore any files in the Archive's Restore directory
if [ -d "$SERVER_RESTORE_DIR" ]; then
    if [ "$(ls -A $SERVER_RESTORE_DIR)" ]; then
	{
	    echo "Restoring files from the Archive..."
	    mkdir $USER_RESTORE_DIR &> /dev/null
	    # Build and run command
	    rsync $ARC_OPTS $EXCLUDES $SERVER_RESTORE_DIR $USER_RESTORE_DIR
	    exit_status=$?
	    if [ $exit_status -ne 0 ]; then
		echo "*** ERROR ***: An rsync error occured while restoring Archive files."
		echo "               rsync Exit Value: $exit_status"
	    fi
	    echo "Finished restoring files from the Archive."
	} >> $MEDIA/.backuplog 2>&1
    else
	echo "No files to restore from Archive." >> $MEDIA/.backuplog
    fi
fi


#----------------------------------------------------------------------
#--- Sync everything else besides the Archive folder ---
#
# Space-separated list of directories to back up
DIRS=$MEDIA/
#
# Options to pass to rsync for non-archive folders
OPTS="-avAH --specials --delete"
#
# Backup directory for non-archive folders
BACKUPDIR="hanxiaox@hpcc.msu.edu:/mnt/research/icer-media-backup"

# Backup the non-Archive folders and record progress in the log file
{
    echo "Starting backup of non-Archive folders..."
    # Build and run command
    rsync $OPTS $EXCLUDES --exclude=Archive $DIRS $BACKUPDIR
    exit_status=$?
    if [ $exit_status -ne 0 ]; then
	echo "*** ERROR ***: An rsync error occured while backing up non-Archive folders."
	echo "               rsync Exit Value: $exit_status"
    fi
    echo "Finished backup of non-Archive folders."
} >> $MEDIA/.backuplog 2>&1


#----------------------------------------------------------------------
# End the backup log file entry
#----------------------------------------------------------------------
{
    echo "Finished backup of Media Drive."
    echo "----------------------------------------------------------------------"
} >> $MEDIA/.backuplog

